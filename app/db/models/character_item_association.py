import uuid
from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import mapped_column, Mapped

from .base import Base


class CharacterItemAssociation(Base):
    __table_args__ = (
        UniqueConstraint(
            "user_character_id",
            "item_id",
            name="idx_unique_character_item",
        ),
    )

    user_character_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user_character_table.id", ondelete="CASCADE")
    )
    item_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("item_table.id", ondelete="CASCADE")
    )
