import smtplib
from email.message import EmailMessage

from celery import Celery
from app.core.config import settings

SMTP_HOST = settings.SMTP_HOST
SMTP_PORT = settings.SMTP_PORT
SMTP_FROM = settings.SMTP_FROM
SMTP_PASSWORD = settings.SMTP_PASSWORD

celery = Celery("tasks", broker=f"redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}")


def get_email_template_dashboard(
    subject: str,
    user_email: str,
    message: str,
):
    email = EmailMessage()
    email["Subject"] = subject
    email["From"] = SMTP_FROM
    email["To"] = user_email

    email.set_content(message)
    return email


@celery.task
def send_email_report_dashboard(
    subject: str,
    user_email: str,
    message: str,
):
    email = get_email_template_dashboard(
        subject=subject,
        user_email=user_email,
        message=message,
    )
    with smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT) as server:
        server.login(SMTP_FROM, SMTP_PASSWORD)
        server.send_message(email)
