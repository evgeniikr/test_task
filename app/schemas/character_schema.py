import uuid

from pydantic import BaseModel, ConfigDict


class TuneModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)


class ShowCharacter(TuneModel):
    id: uuid.UUID
    name: str
    base_health_points: int
    base_speed: int
    base_damage: int


class CharacterCreate(BaseModel):
    name: str
    base_health_points: int
    base_speed: int
    base_damage: int


class ShowUserCharacter(TuneModel):
    id: uuid.UUID
    level: int
    health_points: int
    speed: int
    damage: int
