from typing import Union
from uuid import UUID

from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import User, Balance, BalanceHistory
from app.db.models.user import PortalRole
from app.tasks.tasks import send_email_report_dashboard


class UserRepository:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def create_user(
        self,
        username: str,
        email: str,
        hashed_password: str,
        roles: list[PortalRole],
    ) -> User:
        new_user = User(
            username=username,
            email=email,
            hashed_password=hashed_password,
            roles=roles,
        )
        new_balance = Balance(user=new_user)
        self.session.add(new_user)
        self.session.add(new_balance)
        await self.session.commit()
        return new_user

    async def get_user_by_id(self, user_id: UUID) -> Union[User, None]:
        stmt = select(User).where(User.id == user_id)
        res = await self.session.execute(stmt)
        user = res.scalar_one_or_none()
        if user is not None:
            return user

    async def get_user_by_email(self, email: str) -> Union[User, None]:
        stmt = select(User).where(User.email == email)
        res = await self.session.execute(stmt)
        user = res.scalar_one_or_none()
        if user is not None:
            return user

    async def top_up_balance(
        self,
        user_id: UUID,
        add_gold: int,
        user_email: str,
    ) -> Union[int, None]:
        stmt = (
            update(Balance)
            .where(Balance.user_id == user_id)
            .values(gold=Balance.gold + add_gold)
            .returning(Balance.gold)
        )
        res = await self.session.execute(stmt)
        updated_balance = res.scalar()
        history_record = BalanceHistory(
            change=add_gold, detail="Replenishment of balance", user_id=user_id
        )
        self.session.add(history_record)
        await self.session.commit()

        """Notification by email about replenishment"""
        send_email_report_dashboard.delay(
            subject="Replenishment",
            user_email=user_email,
            message=f"Your balance has been replenished by {add_gold} gold",
        )
        if updated_balance is not None:
            return add_gold

    async def get_balance_history(self, user_id: UUID) -> list[BalanceHistory]:
        stmt = (
            select(BalanceHistory)
            .where(BalanceHistory.user_id == user_id)
            .order_by(BalanceHistory.created_at.desc())
        )
        res = await self.session.execute(stmt)
        balance_history = res.scalars().all()
        return list(balance_history)
