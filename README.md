## Quick Start
* Clone this repository

```bash
git clone https://gitlab.com/evgeniikr/test_task
```
* Go inside of directory
* Run docker-compose.yml
```bash
cd test_task && docker-compose up
```
## Usage
Go to swagger with documentation [docs](http://0.0.0.0:9999/docs/)