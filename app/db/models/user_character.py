import uuid
from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey

from app.db.models.base import Base

if TYPE_CHECKING:
    from .user import User
    from .character import Character
    from .item import Item


class UserCharacter(Base):
    level: Mapped[int] = mapped_column(
        default=1,
        server_default="1",
    )
    health_points: Mapped[int] = mapped_column(
        default=100,
        server_default="100",
    )
    speed: Mapped[int] = mapped_column(
        default=1,
        server_default="1",
    )
    damage: Mapped[int] = mapped_column(
        default=10,
        server_default="10",
    )
    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user_table.id", ondelete="CASCADE")
    )
    character_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("character_table.id", ondelete="CASCADE")
    )

    user: Mapped["User"] = relationship(back_populates="characters")
    character: Mapped["Character"] = relationship(back_populates="users")

    items: Mapped[list["Item"]] = relationship(
        secondary="character_item_association_table",
        back_populates="user_characters",
    )
