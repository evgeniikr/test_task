from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING

from sqlalchemy import ARRAY, DateTime, func
from sqlalchemy import Boolean
from sqlalchemy import String
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from .base import Base


if TYPE_CHECKING:
    from .balance import Balance
    from .user_character import UserCharacter
    from .item import Item

    from .balance_history import BalanceHistory


class PortalRole(str, Enum):
    ROLE_PORTAL_USER = "ROLE_PORTAL_USER"
    ROLE_PORTAL_ADMIN = "ROLE_PORTAL_ADMIN"
    ROLE_PORTAL_SUPERADMIN = "ROLE_PORTAL_SUPERADMIN"


class User(Base):
    email: Mapped[str] = mapped_column(
        String(50),
        nullable=False,
        unique=True,
    )
    hashed_password: Mapped[str] = mapped_column(
        String(512),
        nullable=False,
    )
    username: Mapped[str] = mapped_column(
        String(20),
        nullable=False,
    )
    is_active: Mapped[bool] = mapped_column(
        Boolean,
        default=True,
    )
    roles: Mapped[list] = mapped_column(
        ARRAY(String),
        nullable=False,
    )
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        server_default=func.now(),
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        server_default=func.now(),
        server_onupdate=func.now(),
    )

    balance: Mapped["Balance"] = relationship(
        back_populates="user",
        uselist=False,
    )
    balance_history: Mapped[list["BalanceHistory"]] = relationship(
        back_populates="user", cascade="all, delete-orphan"
    )

    characters: Mapped[list["UserCharacter"]] = relationship(
        back_populates="user", cascade="all, delete-orphan"
    )
    items: Mapped[list["Item"]] = relationship(
        secondary="user_item_association_table",
        back_populates="users",
    )

    @property
    def is_admin(self) -> bool:
        return PortalRole.ROLE_PORTAL_ADMIN in self.roles

    @property
    def is_superadmin(self) -> bool:
        return PortalRole.ROLE_PORTAL_SUPERADMIN in self.roles
