from typing import Union
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.db.models import (
    User,
    Item,
    Balance,
    BalanceHistory,
)
from app.tasks.tasks import send_email_report_dashboard


class ItemRepository:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def create_item(
        self,
        item_name: str,
        item_type: str,
        health_points_bonus: int,
        speed_bonus: int,
        damage_bonus: int,
        cost: int,
    ) -> Item:
        new_item = Item(
            item_name=item_name,
            item_type=item_type,
            health_points_bonus=health_points_bonus,
            speed_bonus=speed_bonus,
            damage_bonus=damage_bonus,
            cost=cost,
        )
        self.session.add(new_item)
        await self.session.commit()
        return new_item

    async def get_item_by_id(self, item_id: UUID) -> Union[Item, None]:
        stmt = select(Item).where(Item.id == item_id)
        res = await self.session.execute(stmt)
        item = res.scalar_one_or_none()
        if item is not None:
            return item

    async def get_user_item_by_id(
        self, user_id: UUID, item_id: UUID
    ) -> Union[Item, None]:
        stmt = (
            select(Item).join(User.items).where(User.id == user_id, Item.id == item_id)
        )
        res = await self.session.execute(stmt)
        item = res.scalar_one_or_none()
        if item is not None:
            return item

    async def get_all_items(
        self,
        offset: int,
        per_page: int,
    ) -> list[Item]:
        stmt = select(Item).offset(offset).limit(per_page)
        res = await self.session.execute(stmt)
        items = res.scalars().all()
        return list(items)

    async def get_user_balance(self, user_id: UUID) -> Union[Balance, None]:
        stmt = select(Balance).where(Balance.user_id == user_id)
        res = await self.session.execute(stmt)
        balance = res.scalars().one_or_none()
        if balance is not None:
            return balance

    async def add_item_for_user(
        self,
        user_id: UUID,
        item: Item,
        user_balance: Balance,
    ) -> Item:
        items_of_user = await self.session.scalar(
            select(User).options(selectinload(User.items)).where(User.id == user_id)
        )
        items_of_user.items.append(item)
        user_balance.gold -= item.cost
        history_record = BalanceHistory(
            change=item.cost,
            detail=f"Write-off for purchase of {item.item_name}",
            user_id=user_id,
        )
        self.session.add(history_record)
        await self.session.commit()

        """Notification by email about write-off"""
        send_email_report_dashboard.delay(
            subject="Write-off",
            user_email=items_of_user.email,
            message=f"A item {item.item_name} purchase was made in the amount of {item.cost} gold",
        )
        return item

    async def send_item_to_user(
        self,
        current_user_id: UUID,
        target_user_id: UUID,
        item: Item,
        user_balance: Balance,
    ) -> Item:
        sender = await self.session.scalar(
            select(User)
            .options(selectinload(User.items))
            .where(User.id == current_user_id)
        )
        receiver = await self.session.scalar(
            select(User)
            .options(selectinload(User.items))
            .where(User.id == target_user_id)
        )
        sender.items.remove(item)
        receiver.items.append(item)
        commission = item.cost * 0.05
        user_balance.gold -= commission
        history_record = BalanceHistory(
            change=commission,
            detail=f"Write-off for transfer of {item.item_name} to {target_user_id}",
            user_id=current_user_id,
        )
        self.session.add(history_record)
        await self.session.commit()

        send_email_report_dashboard.delay(  # Notification by email about write-off
            subject="Write-off",
            user_email=sender.email,
            message=f"A commission has been debited from your balance in the amount of {commission} gold",
        )
        return item

    async def drop_user_item(
        self,
        target_user_id: UUID,
        item: Item,
    ) -> Item:
        user_items = await self.session.scalar(
            select(User)
            .options(selectinload(User.items))
            .where(User.id == target_user_id)
        )
        user_items.items.remove(item)
        await self.session.commit()

        send_email_report_dashboard.delay(  # Notification by email about item drop
            subject="Item drop",
            user_email=user_items.email,
            message=f"Your item {item.item_name} has been dropped",
        )
        return item
