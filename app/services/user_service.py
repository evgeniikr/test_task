from typing import Union
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import BalanceHistory, Item
from app.repositories.user_repository import UserRepository
from app.schemas.item_schema import ShowItem
from app.schemas.user_schema import (
    UserCreate,
    ShowUser,
    ShowBalance,
    ShowBalanceHistory,
)
from app.utils.auth.hashing import Hasher
from app.db.models.user import PortalRole, User


async def _create_user(
    payload: UserCreate,
    session: AsyncSession,
) -> ShowUser:
    user_rep = UserRepository(session)
    user = await user_rep.create_user(
        username=payload.username,
        email=payload.email,
        hashed_password=Hasher.get_password_hash(payload.password),
        roles=[
            PortalRole.ROLE_PORTAL_USER,
        ],
    )
    return ShowUser(
        id=user.id,
        username=user.username,
        email=user.email,
        is_active=user.is_active,
        created_at=user.created_at,
        updated_at=user.updated_at,
    )


async def _get_user_by_id(
    user_id: UUID,
    session: AsyncSession,
) -> Union[User, None]:
    user_rep = UserRepository(session)
    user = await user_rep.get_user_by_id(user_id=user_id)
    if user is not None:
        return user


async def _top_up_balance(
    user_id: UUID,
    add_gold: int,
    user_email: str,
    session: AsyncSession,
) -> ShowBalance:
    user_rep = UserRepository(session)
    new_balance = await user_rep.top_up_balance(
        user_id=user_id,
        add_gold=add_gold,
        user_email=user_email,
    )
    return ShowBalance(id=user_id, new_balance=new_balance)


async def _get_balance_history(
    user_id: UUID,
    session: AsyncSession,
) -> list[ShowBalanceHistory]:
    user_rep = UserRepository(session)
    balance_history = await user_rep.get_balance_history(user_id=user_id)
    return [ShowBalanceHistory.from_orm(history) for history in balance_history]
