import pytest
from httpx import AsyncClient

from tests.conftest import (
    client,
)


async def test_create_user(ac: AsyncClient):
    user_data = {
        "email": "test@gmail.com",
        "password": "testtest",
        "username": "test",
    }
    resp = client.post("/v1/users/", json=user_data)
    data_from_resp = resp.json()
    assert resp.status_code == 200
    assert data_from_resp["username"] == user_data["username"]
    assert data_from_resp["email"] == user_data["email"]
    assert data_from_resp["is_active"] is True


async def test_create_user_duplicate_email_error(ac: AsyncClient):
    user_data_same = {
        "email": "test@gmail.com",
        "password": "testtest",
        "username": "test",
    }
    resp = client.post("/v1/users/", json=user_data_same)
    assert resp.status_code == 503


@pytest.mark.parametrize(
    "user_data_for_creation, expected_status_code, expected_detail",
    [
        (
            {
                "username": "name1",
                "email": "email@gmial.com",
                "password": "password123",
            },
            422,
            {"detail": "Username should contains only letters"},
        ),
        (
            {
                "username": "ValidUsername",
                "email": "invalid_email",
                "password": "password123",
            },
            422,
            {
                "detail": [
                    {
                        "type": "value_error",
                        "loc": ["body", "email"],
                        "msg": "value is not a valid email address: The email address is not valid. It must have exactly one @-sign.",
                        "input": "invalid_email",
                        "ctx": {
                            "reason": "The email address is not valid. It must have exactly one @-sign."
                        },
                    }
                ]
            },
        ),
        (
            {
                "username": "ValidUsername",
                "email": "test@example.com",
                "password": "short",
            },
            422,
            {"detail": "Password is too short"},
        ),
    ],
)
async def test_create_user_validation_error(
    ac: AsyncClient, user_data_for_creation, expected_status_code, expected_detail
):
    resp = client.post("/v1/users/", json=user_data_for_creation)
    data_from_resp = resp.json()
    assert resp.status_code == expected_status_code
    assert data_from_resp == expected_detail
