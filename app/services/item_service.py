from typing import Union
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import Balance, Item
from app.repositories.item_repository import ItemRepository
from app.schemas.item_schema import (
    ShowItem,
    ItemCreate,
)


async def _create_item(
    payload: ItemCreate,
    session: AsyncSession,
) -> ShowItem:
    item_rep = ItemRepository(session)
    item = await item_rep.create_item(
        item_name=payload.item_name,
        item_type=payload.item_type,
        health_points_bonus=payload.health_points_bonus,
        speed_bonus=payload.speed_bonus,
        damage_bonus=payload.damage_bonus,
        cost=payload.cost,
    )
    return ShowItem(
        id=item.id,
        item_name=item.item_name,
        item_type=item.item_type,
        health_points_bonus=item.health_points_bonus,
        speed_bonus=item.speed_bonus,
        damage_bonus=item.damage_bonus,
        cost=item.cost,
    )


async def _get_item_by_id(
    item_id: UUID,
    session: AsyncSession,
) -> Union[Item, None]:
    item_rep = ItemRepository(session)
    item = await item_rep.get_item_by_id(item_id=item_id)
    if item is not None:
        return item


async def _get_user_item_by_id(
    user_id: UUID,
    item_id: UUID,
    session: AsyncSession,
) -> Union[Item, None]:
    item_rep = ItemRepository(session)
    item = await item_rep.get_user_item_by_id(
        user_id=user_id,
        item_id=item_id,
    )
    if item is not None:
        return item


async def _get_all_items(
    offset: int,
    per_page: int,
    session: AsyncSession,
) -> list[ShowItem]:
    item_rep = ItemRepository(session)
    items = await item_rep.get_all_items(
        offset=offset,
        per_page=per_page,
    )
    return [ShowItem.from_orm(item) for item in items]


async def _get_user_balance(
    user_id: UUID,
    session: AsyncSession,
) -> Union[Balance, None]:
    balance_rep = ItemRepository(session)
    balance = await balance_rep.get_user_balance(user_id=user_id)
    if balance is not None:
        return balance


async def _add_item_for_user(
    user_id: UUID,
    item: Item,
    user_balance: Balance,
    session: AsyncSession,
) -> ShowItem:
    item_rep = ItemRepository(session)
    item = await item_rep.add_item_for_user(
        user_id=user_id,
        item=item,
        user_balance=user_balance,
    )
    return ShowItem(
        id=item.id,
        item_name=item.item_name,
        item_type=item.item_type,
        health_points_bonus=item.health_points_bonus,
        speed_bonus=item.speed_bonus,
        damage_bonus=item.damage_bonus,
        cost=item.cost,
    )


async def _send_item_to_user(
    current_user_id: UUID,
    target_user_id: UUID,
    item: Item,
    user_balance: Balance,
    session: AsyncSession,
) -> ShowItem:
    item_rep = ItemRepository(session)
    sent_item = await item_rep.send_item_to_user(
        target_user_id=target_user_id,
        current_user_id=current_user_id,
        item=item,
        user_balance=user_balance,
    )
    return ShowItem(
        id=sent_item.id,
        item_name=sent_item.item_name,
        item_type=sent_item.item_type,
        health_points_bonus=sent_item.health_points_bonus,
        speed_bonus=sent_item.speed_bonus,
        damage_bonus=sent_item.damage_bonus,
        cost=sent_item.cost,
    )


async def _drop_user_item(
    target_user_id: UUID,
    item: Item,
    session: AsyncSession,
) -> ShowItem:
    item_rep = ItemRepository(session)
    deleted_item = await item_rep.drop_user_item(
        target_user_id=target_user_id,
        item=item,
    )
    return ShowItem(
        id=deleted_item.id,
        item_name=deleted_item.item_name,
        item_type=deleted_item.item_type,
        health_points_bonus=deleted_item.health_points_bonus,
        speed_bonus=deleted_item.speed_bonus,
        damage_bonus=deleted_item.damage_bonus,
        cost=deleted_item.cost,
    )
