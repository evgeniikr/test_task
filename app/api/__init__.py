from fastapi import APIRouter

from app.api.v1.admin_endpoints import admin_router
from app.api.v1.endpoints import user_router

router = APIRouter()
router.include_router(router=user_router, prefix="/v1/users")
router.include_router(router=admin_router, prefix="/v1/admins")
