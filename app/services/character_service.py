from typing import Union
from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import Character, User, UserCharacter, Item
from app.repositories.character_repository import CharacterRepository
from app.schemas.character_schema import (
    CharacterCreate,
    ShowCharacter,
    ShowUserCharacter,
)


async def _create_character(
    payload: CharacterCreate,
    session: AsyncSession,
) -> ShowCharacter:
    character_rep = CharacterRepository(session)
    character = await character_rep.create_character(
        name=payload.name,
        base_health_points=payload.base_health_points,
        base_speed=payload.base_speed,
        base_damage=payload.base_damage,
    )
    return ShowCharacter(
        id=character.id,
        name=character.name,
        base_health_points=character.base_health_points,
        base_speed=character.base_speed,
        base_damage=character.base_damage,
    )


async def _get_character_by_id(
    character_id: UUID,
    session: AsyncSession,
) -> Union[Character, None]:
    character_rep = CharacterRepository(session)
    character = await character_rep.get_character_by_id(character_id=character_id)
    if character is not None:
        return character


async def _get_user_character_by_id(
    user_character_id: UUID,
    session: AsyncSession,
) -> Union[UserCharacter, None]:
    character_rep = CharacterRepository(session)
    user_character = await character_rep.get_user_character_by_id(
        user_character_id=user_character_id,
    )
    if user_character is not None:
        return user_character


async def _get_all_characters(
    offset: int,
    per_page: int,
    session: AsyncSession,
) -> list[ShowCharacter]:
    character_rep = CharacterRepository(session)
    characters = await character_rep.get_all_characters(
        offset=offset,
        per_page=per_page,
    )
    return [ShowCharacter.from_orm(character) for character in characters]


async def _create_character_for_user(
    user: User,
    character: Character,
    session: AsyncSession,
) -> ShowUserCharacter:
    character_rep = CharacterRepository(session)
    user_character = await character_rep.create_character_for_user(
        user=user,
        character=character,
    )
    return ShowUserCharacter(
        id=user_character.id,
        level=user_character.level,
        health_points=user_character.health_points,
        speed=user_character.speed,
        damage=user_character.damage,
    )


async def _put_on_or_take_off_item_for_character(
    user_id: UUID,
    user_character_id: UUID,
    item: Item,
    put_on: bool,
    session: AsyncSession,
) -> ShowUserCharacter:
    item_rep = CharacterRepository(session)
    character = await item_rep.put_on_or_take_off_item_for_character(
        user_id=user_id,
        user_character_id=user_character_id,
        put_on=put_on,
        item=item,
    )
    return ShowUserCharacter(
        id=character.id,
        level=character.level,
        health_points=character.health_points,
        speed=character.speed,
        damage=character.damage,
    )
