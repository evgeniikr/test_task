import re
import uuid
from datetime import datetime

from fastapi import HTTPException
from pydantic import BaseModel, ConfigDict
from pydantic import EmailStr
from pydantic import field_validator

LETTER_MATCH_PATTERN = re.compile(r"^[а-яА-Яa-zA-Z\-]+$")


class TuneModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)


class ShowUser(TuneModel):
    id: uuid.UUID
    email: EmailStr
    username: str
    is_active: bool
    created_at: datetime
    updated_at: datetime


class UserCreate(BaseModel):
    username: str
    email: EmailStr
    password: str

    @field_validator("username")
    @classmethod
    def validate_username(cls, value):
        if not LETTER_MATCH_PATTERN.match(value):
            raise HTTPException(
                status_code=422, detail="Username should contains only letters"
            )
        return value

    @field_validator("password")
    @classmethod
    def validate_password(cls, value):
        if len(value) < 6:
            raise HTTPException(status_code=422, detail="Password is too short")
        return value


class Token(BaseModel):
    access_token: str
    token_type: str


class ShowBalance(TuneModel):
    id: uuid.UUID
    new_balance: int


class ShowBalanceHistory(TuneModel):
    id: uuid.UUID
    change: int
    detail: str
    created_at: datetime
