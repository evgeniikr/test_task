import uuid
from pydantic import BaseModel, ConfigDict

from app.db.models import Character, Item


class TuneModel(BaseModel):
    model_config = ConfigDict(from_attributes=True)


class ShowItem(TuneModel):
    id: uuid.UUID
    item_name: str
    item_type: str
    health_points_bonus: int
    speed_bonus: int
    damage_bonus: int
    cost: int


class ItemCreate(BaseModel):
    item_name: str
    item_type: str
    health_points_bonus: int
    speed_bonus: int
    damage_bonus: int
    cost: int
