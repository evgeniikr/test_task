from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession
from app.db.database import get_db
import aio_pika
import json
import asyncio

from app.services.user_service import _get_balance_history


async def on_request(
    message: aio_pika.IncomingMessage, session: AsyncSession = get_db()
):
    async with message.process():
        request_data = json.loads(message.body.decode())
        user_id = UUID(request_data["user_id"])

        balance_history = await _get_balance_history(user_id, session)

        response_message = aio_pika.Message(
            body=json.dumps([record.dict() for record in balance_history]).encode(),
            correlation_id=message.correlation_id,
        )
        await message.reply_to_channel.default_exchange.publish(
            response_message, routing_key=message.reply_to
        )


async def main():
    connection = await aio_pika.connect_robust("amqp://guest:guest@localhost/")
    async with connection:
        channel = await connection.channel()
        rpc_queue = await channel.declare_queue("rpc_queue")

        await rpc_queue.consume(on_request)

        print("Awaiting RPC requests")
        await asyncio.Future()  # Run forever


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(main())
