from uuid import uuid4

import pytest
from httpx import AsyncClient

from app.db.models.user import PortalRole
from tests.conftest import (
    client,
    create_test_auth_headers_for_user,
    create_user_in_database,
)


async def test_get_user(ac: AsyncClient):
    user_data = {
        "user_id": uuid4(),
        "username": "testname",
        "email": "test@example.com",
        "hashed_password": "testtest",
        "is_active": True,
        "roles": [PortalRole.ROLE_PORTAL_USER],
    }
    await create_user_in_database(**user_data)
    resp = client.get(
        f"/v1/users/{user_data['user_id']}",
        headers=create_test_auth_headers_for_user(user_data["email"]),
    )
    assert resp.status_code == 200
    user_from_response = resp.json()
    assert user_from_response["id"] == str(user_data["user_id"])
    assert user_from_response["username"] == user_data["username"]
    assert user_from_response["email"] == user_data["email"]
    assert user_from_response["is_active"] == user_data["is_active"]


async def test_get_user_id_validation_error(ac: AsyncClient):
    user_data = {
        "user_id": uuid4(),
        "username": "testname",
        "email": "test@example.com",
        "hashed_password": "testtest",
        "is_active": True,
        "roles": [PortalRole.ROLE_PORTAL_USER],
    }
    resp = client.get(
        f"/v1/users/{12345}",
        headers=create_test_auth_headers_for_user(user_data["email"]),
    )
    assert resp.status_code == 422
    data_from_response = resp.json()
    assert data_from_response == {
        "detail": [
            {
                "type": "uuid_parsing",
                "loc": ["path", "user_id"],
                "msg": "Input should be a valid UUID, invalid length: expected length 32 for simple format, found 5",
                "input": "12345",
                "ctx": {
                    "error": "invalid length: expected length 32 for simple format, found 5"
                },
            }
        ]
    }


async def test_get_user_not_found(ac: AsyncClient):
    user_data = {
        "user_id": uuid4(),
        "username": "testname",
        "email": "test@example.com",
        "hashed_password": "testtest",
        "is_active": True,
        "roles": [PortalRole.ROLE_PORTAL_USER],
    }
    user_id_for_finding = uuid4()
    resp = client.get(
        f"/v1/users/{user_id_for_finding}",
        headers=create_test_auth_headers_for_user(user_data["email"]),
    )
    assert resp.status_code == 404
    assert resp.json() == {"detail": f"User with id {user_id_for_finding} not found"}


async def test_get_user_unauth_error(ac: AsyncClient):
    user_data = {
        "user_id": uuid4(),
        "username": "testname",
        "email": "test@example.com",
        "hashed_password": "testtest",
        "is_active": True,
        "roles": [PortalRole.ROLE_PORTAL_USER],
    }
    user_id_for_finding = uuid4()
    resp = client.get(
        f"/v1/users/{user_id_for_finding}",
    )
    assert resp.status_code == 401
    assert resp.json() == {"detail": "Not authenticated"}


async def test_get_user_bad_cred(ac: AsyncClient):
    user_data = {
        "user_id": uuid4(),
        "username": "testname",
        "email": "test@example.com",
        "hashed_password": "testtest",
        "is_active": True,
        "roles": [PortalRole.ROLE_PORTAL_USER],
    }
    user_id = uuid4()
    resp = client.get(
        f"/v1/users/{user_id}",
        headers=create_test_auth_headers_for_user(user_data["email"] + "q"),
    )
    assert resp.status_code == 401
    assert resp.json() == {"detail": "Could not available credentials"}
