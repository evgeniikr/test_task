from datetime import timedelta
from typing import AsyncGenerator

import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient, ASGITransport
from sqlalchemy.ext.asyncio import (
    AsyncSession,
    create_async_engine,
    async_sessionmaker,
)
from sqlalchemy.pool import NullPool

from app.core.config import settings
from app.db.database import get_db
from app.db.models import Base
from app.db.models.user import PortalRole, User
from app.utils.auth.security import create_access_token
from main import app

# DATABASE
DATABASE_URL_TEST = settings.TEST_DATABASE_URL_asyncpg

engine_test = create_async_engine(DATABASE_URL_TEST, poolclass=NullPool)
async_session_maker = async_sessionmaker(
    bind=engine_test,
    class_=AsyncSession,
    expire_on_commit=False,
)


async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session_maker() as session:
        yield session


app.dependency_overrides[get_db] = override_get_async_session


@pytest.fixture(autouse=True, scope="session")
async def prepare_database():
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


client = TestClient(app)


@pytest.fixture(scope="session")
async def ac() -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(
        transport=ASGITransport(app=app), base_url="http://tests"
    ) as ac:
        yield ac


async def create_user_in_database(
    user_id: str,
    username: str,
    email: str,
    is_active: bool,
    hashed_password: str,
    roles: list[PortalRole],
):
    async with async_session_maker() as session:
        async with session.begin():
            user = User(
                id=user_id,
                username=username,
                email=email,
                is_active=is_active,
                hashed_password=hashed_password,
                roles=roles,
            )
            session.add(user)
            await session.commit()


def create_test_auth_headers_for_user(email: str) -> dict[str, str]:
    access_token = create_access_token(
        data={"sub": email},
        expire_delta=timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES),
    )
    return {"Authorization": f"Bearer {access_token}"}
