import uuid
from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import mapped_column, Mapped

from .base import Base


class UserItemAssociation(Base):
    __table_args__ = (
        UniqueConstraint(
            "user_id",
            "item_id",
            name="idx_unique_user_item",
        ),
    )

    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user_table.id", ondelete="CASCADE")
    )
    item_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("item_table.id", ondelete="CASCADE")
    )
