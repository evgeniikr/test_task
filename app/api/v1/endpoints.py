import json
import uuid
from datetime import timedelta
from uuid import UUID

import aio_pika
from fastapi import APIRouter, Query
from fastapi import Depends
from fastapi import HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_cache.decorator import cache
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status


from app.core.config import settings
from app.db.models import User
from app.schemas.character_schema import ShowCharacter, ShowUserCharacter
from app.schemas.item_schema import (
    ShowItem,
)
from app.schemas.user_schema import (
    UserCreate,
    ShowUser,
    Token,
    ShowBalanceHistory,
)
from app.services.character_service import (
    _get_character_by_id,
    _get_all_characters,
    _put_on_or_take_off_item_for_character,
    _get_user_character_by_id,
    _create_character_for_user,
)
from app.services.item_service import (
    _get_item_by_id,
    _get_user_balance,
    _send_item_to_user,
    _get_all_items,
    _add_item_for_user,
    _get_user_item_by_id,
)
from app.services.user_service import (
    _create_user,
    _get_user_by_id,
    _get_balance_history,
)
from app.db.database import get_db
from app.utils.auth.auth import authenticate_user, get_current_user_with_token
from app.utils.auth.security import create_access_token

user_router = APIRouter(tags=["User"])


@user_router.post("/", response_model=ShowUser)
async def create_user(
    payload: UserCreate,
    session: AsyncSession = Depends(get_db),
) -> ShowUser:
    try:
        return await _create_user(
            payload=payload,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session: AsyncSession = Depends(get_db),
):
    user = await authenticate_user(
        form_data.username,
        form_data.password,
        session=session,
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.email, "other_custom_data": "other data"},
        expire_delta=access_token_expires,
    )
    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@user_router.get("/{user_id}", response_model=ShowUser)
async def get_user_by_id(
    user_id: UUID,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowUser:
    user = await _get_user_by_id(
        user_id=user_id,
        session=session,
    )
    if user is None:
        raise HTTPException(
            status_code=404,
            detail=f"User with id {user_id} not found",
        )
    return user


@user_router.get("/{user_id}/characters", response_model=list[ShowCharacter])
@cache(expire=3600)
async def get_all_characters(
    user_id: UUID,
    page: int = Query(1, ge=1, description="Page number"),
    per_page: int = Query(10, ge=1, le=100, description="Number of elements per page"),
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> list[ShowCharacter]:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    try:
        offset = (page - 1) * per_page
        return await _get_all_characters(
            offset=offset,
            per_page=per_page,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.post("/{user_id}/characters/", response_model=ShowUserCharacter)
async def create_character_for_user(
    user_id: UUID,
    character_id: UUID,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowUserCharacter:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    user = await _get_user_by_id(
        user_id=user_id,
        session=session,
    )
    if user is None:
        raise HTTPException(
            status_code=404,
            detail=f"User with id {user_id} not found",
        )
    character = await _get_character_by_id(
        character_id=character_id,
        session=session,
    )
    if not character:
        raise HTTPException(
            status_code=404, detail=f"Character {character_id} doesn't exist"
        )
    try:
        return await _create_character_for_user(
            user=user,
            character=character,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.get("/{user_id}/items", response_model=list[ShowItem])
@cache(expire=3600)
async def get_all_items(
    user_id: UUID,
    session: AsyncSession = Depends(get_db),
    page: int = Query(1, ge=1, description="Page number"),
    per_page: int = Query(10, ge=1, le=100, description="Number of elements per page"),
    current_user: User = Depends(get_current_user_with_token),
) -> list[ShowItem]:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    try:
        offset = (page - 1) * per_page
        return await _get_all_items(
            offset=offset,
            per_page=per_page,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.post("/{user_id}/items/", response_model=ShowItem)
async def add_item_for_user(
    user_id: UUID,
    item_id: UUID,
    current_user: User = Depends(get_current_user_with_token),
    session: AsyncSession = Depends(get_db),
) -> ShowItem:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    item = await _get_item_by_id(
        item_id=item_id,
        session=session,
    )
    if not item:
        raise HTTPException(status_code=404, detail=f"Item {item_id} doesn't exist")
    user_balance = await _get_user_balance(
        user_id=current_user.id,
        session=session,
    )
    if item.cost > user_balance.gold:
        raise HTTPException(
            status_code=400,
            detail=f"You're missing {item.cost-user_balance.gold} gold",
        )
    try:
        return await _add_item_for_user(
            user_id=current_user.id,
            item=item,
            user_balance=user_balance,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.patch(
    "/{user_id}/characters/{user_character_id}/items",
    response_model=ShowUserCharacter,
)
async def put_on_or_take_off_item_for_character(
    user_id: UUID,
    item_id: UUID,
    user_character_id: UUID,
    put_on: bool = True,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowUserCharacter:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    item = await _get_item_by_id(
        item_id=item_id,
        session=session,
    )
    if not item:
        raise HTTPException(status_code=404, detail=f"You don't have item {item_id}")
    user_character = await _get_user_character_by_id(
        user_character_id=user_character_id,
        session=session,
    )
    if not user_character:
        raise HTTPException(
            status_code=404, detail=f"You don't have character {user_character_id}"
        )
    try:
        return await _put_on_or_take_off_item_for_character(
            user_id=user_id,
            user_character_id=user_character_id,
            put_on=put_on,
            item=item,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.post(
    "/{user_id}/items/{item_id}/transfer",
    response_model=ShowItem,
)
async def send_item_to_user(
    item_id: UUID,
    user_id: UUID,
    target_user_id: UUID,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowItem:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    if user_id == target_user_id:
        raise HTTPException(
            status_code=403, detail="It's forbidden to send item to yourself"
        )
    item = await _get_user_item_by_id(
        user_id=user_id,
        item_id=item_id,
        session=session,
    )
    if not item:
        raise HTTPException(status_code=404, detail=f"Item {item_id} doesn't exist")
    user_balance = await _get_user_balance(
        user_id=current_user.id,
        session=session,
    )

    if (
        item.cost * 0.05 > user_balance.gold
    ):  # Check gold of user to write off the commission
        raise HTTPException(
            status_code=400,
            detail=f"You're missing {item.cost * 0.05 - user_balance.gold} gold to send this item",
        )
    target_user = await _get_user_by_id(
        user_id=target_user_id,
        session=session,
    )
    if not target_user:
        raise HTTPException(status_code=404, detail=f"User {target_user} doesn't exist")
    try:
        return await _send_item_to_user(
            current_user_id=user_id,
            target_user_id=target_user_id,
            item=item,
            user_balance=user_balance,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@user_router.get("/{user_id}/balance/history", response_model=list[ShowBalanceHistory])
async def get_balance_history(
    user_id: UUID,
    current_user: User = Depends(get_current_user_with_token),
) -> list[ShowBalanceHistory]:
    if user_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")

    correlation_id = str(uuid.uuid4())
    response_queue_name = f"response_queue_{correlation_id}"

    connection = await aio_pika.connect_robust("amqp://guest:guest@localhost/")
    channel = await connection.channel()

    response_queue = await channel.declare_queue(response_queue_name, exclusive=True)

    request_data = {"user_id": str(user_id)}
    message_body = json.dumps(request_data)
    await channel.default_exchange.publish(
        aio_pika.Message(
            body=message_body.encode(),
            correlation_id=correlation_id,
            reply_to=response_queue_name,
        ),
        routing_key="rpc_queue",
    )

    async with response_queue.iterator() as queue_iter:
        async for message in queue_iter:
            async with message.process():
                if message.correlation_id == correlation_id:
                    response_data = json.loads(message.body.decode())
                    await connection.close()
                    return response_data

    raise HTTPException(status_code=503, detail="Service unavailable")
