import uuid
from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING

from sqlalchemy import ARRAY, DateTime, func, ForeignKey
from sqlalchemy import Boolean
from sqlalchemy import String
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from .base import Base


if TYPE_CHECKING:
    from .user import User


class BalanceHistory(Base):
    change: Mapped[int] = mapped_column()
    detail: Mapped[str] = mapped_column(String(512))
    created_at: Mapped[datetime] = mapped_column(
        DateTime(timezone=True),
        server_default=func.now(),
    )

    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user_table.id", ondelete="CASCADE")
    )
    user: Mapped["User"] = relationship(back_populates="balance_history")
