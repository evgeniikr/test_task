from uuid import UUID

from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import User
from app.schemas.character_schema import CharacterCreate, ShowCharacter
from app.schemas.item_schema import ItemCreate, ShowItem


from app.db.database import get_db
from app.schemas.user_schema import ShowBalance
from app.services.character_service import _create_character
from app.services.item_service import (
    _create_item,
    _drop_user_item,
    _get_user_item_by_id,
)
from app.services.user_service import (
    _get_user_by_id,
    _top_up_balance,
)
from app.utils.auth.auth import get_current_user_with_token

admin_router = APIRouter(tags=["Admin"])


@admin_router.post("/{admin_id}/characters")
async def create_new_character(
    admin_id: UUID,
    payload: CharacterCreate,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowCharacter:
    if admin_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    # if not current_user.is_admin:
    #     raise HTTPException(status_code=403, detail="Forbidden")
    try:
        return await _create_character(
            payload=payload,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@admin_router.post("/{admin_id}/items")
async def create_new_item(
    admin_id: UUID,
    payload: ItemCreate,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowItem:
    if admin_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    # if not current_user.is_admin:
    #     raise HTTPException(status_code=403, detail="Forbidden")
    try:
        return await _create_item(
            payload=payload,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@admin_router.patch("/{admin_id}/users/items")
async def drop_user_item(
    admin_id: UUID,
    target_user_id: UUID,
    item_id: UUID,
    current_user: User = Depends(get_current_user_with_token),
    session: AsyncSession = Depends(get_db),
) -> ShowItem:
    if admin_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    # if not current_user.is_admin:
    #     raise HTTPException(status_code=403, detail="Forbidden")
    target_user = await _get_user_by_id(
        user_id=target_user_id,
        session=session,
    )
    if target_user is None:
        raise HTTPException(
            status_code=404,
            detail=f"User with id {target_user_id} not found",
        )
    item = await _get_user_item_by_id(
        user_id=target_user_id,
        item_id=item_id,
        session=session,
    )
    if not item:
        raise HTTPException(
            status_code=404,
            detail=f"User {target_user.username} don't have this item",
        )
    try:
        return await _drop_user_item(
            target_user_id=target_user_id,
            item=item,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )


@admin_router.patch("/{admin_id}/users/balance", response_model=ShowBalance)
async def top_up_balance(
    admin_id: UUID,
    user_id: UUID,
    add_gold: int,
    session: AsyncSession = Depends(get_db),
    current_user: User = Depends(get_current_user_with_token),
) -> ShowBalance:
    if admin_id != current_user.id:
        raise HTTPException(status_code=403, detail="Forbidden")
    # if not current_user.is_superadmin:
    #     raise HTTPException(status_code=403, detail="Forbidden")
    target_user = await _get_user_by_id(
        user_id=user_id,
        session=session,
    )
    if target_user is None:
        raise HTTPException(
            status_code=404,
            detail=f"User with id {user_id} not found",
        )
    try:
        return await _top_up_balance(
            user_id=user_id,
            add_gold=add_gold,
            user_email=target_user.email,
            session=session,
        )
    except IntegrityError as err:
        raise HTTPException(
            status_code=503,
            detail=f"Database error: {err}",
        )
