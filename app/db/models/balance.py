import uuid
from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from app.db.models.base import Base

if TYPE_CHECKING:
    from .user import User


class Balance(Base):
    gold: Mapped[int] = mapped_column(default=100, server_default="100")

    user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey("user_table.id", ondelete="CASCADE"))

    user: Mapped["User"] = relationship(back_populates="balance")
