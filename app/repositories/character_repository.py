from typing import Union
from uuid import UUID

from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.db.models import Character, User, Item, CharacterItemAssociation, UserCharacter


class CharacterRepository:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def create_character(
        self,
        name: str,
        base_health_points: int,
        base_speed: int,
        base_damage: int,
    ) -> Character:
        new_character = Character(
            name=name,
            base_health_points=base_health_points,
            base_speed=base_speed,
            base_damage=base_damage,
        )
        self.session.add(new_character)
        await self.session.commit()
        return new_character

    async def get_character_by_id(self, character_id) -> Union[Character, None]:
        stmt = select(Character).where(Character.id == character_id)
        res = await self.session.execute(stmt)
        character = res.scalar_one_or_none()
        if character is not None:
            return character

    async def get_user_character_by_id(
        self,
        user_character_id: UUID,
    ) -> Union[UserCharacter, None]:
        stmt = select(UserCharacter).where(UserCharacter.id == user_character_id)
        res = await self.session.execute(stmt)
        user_character = res.scalar_one_or_none()
        if user_character is not None:
            return user_character

    async def get_all_characters(
        self,
        offset: int,
        per_page: int,
    ) -> list[Character]:
        stmt = select(Character).offset(offset).limit(per_page)
        res = await self.session.execute(stmt)
        characters = res.scalars().all()
        return list(characters)

    async def create_character_for_user(
        self,
        user: User,
        character: Character,
    ) -> UserCharacter:
        user_character = UserCharacter(
            user=user,
            character=character,
            health_points=character.base_health_points,
            speed=character.base_speed,
            damage=character.base_damage,
        )
        self.session.add(user_character)
        await self.session.commit()
        return user_character

    async def put_on_or_take_off_item_for_character(
        self,
        user_id: UUID,
        user_character_id: UUID,
        item: Item,
        put_on: bool,
    ) -> UserCharacter:
        user_items = await self.session.scalar(
            select(User).options(selectinload(User.items)).where(User.id == user_id)
        )
        user_character = await self.session.scalar(
            select(UserCharacter)
            .options(selectinload(UserCharacter.items))
            .where(UserCharacter.id == user_character_id)
        )
        if put_on:
            user_character.items.append(item)
            user_items.items.remove(
                item
            )  # Item's removed from the list of items of current user
            factor = 1
        else:
            user_character.items.remove(item)
            user_items.items.append(
                item
            )  # Item's added from the list of items of current user
            factor = -1
        user_character.health_points += (
            item.health_points_bonus * factor
        )  # Attributes recalculation
        user_character.speed += item.speed_bonus * factor
        user_character.damage += item.damage_bonus * factor
        user_character.level = (
            user_character.health_points // 100
        )  # User character achieves new level every 100 hp
        await self.session.commit()
        return user_character
