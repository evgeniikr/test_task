from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship
from sqlalchemy import String

from app.db.models.base import Base

if TYPE_CHECKING:
    from .user_character import UserCharacter


class Character(Base):
    name: Mapped[str] = mapped_column(
        String(50),
        nullable=False,
        unique=True,
    )
    base_health_points: Mapped[int] = mapped_column(
        default=100,
        server_default="100",
    )
    base_speed: Mapped[int] = mapped_column(
        default=1,
        server_default="1",
    )
    base_damage: Mapped[int] = mapped_column(
        default=10,
        server_default="10",
    )

    users: Mapped[list["UserCharacter"]] = relationship(
        back_populates="character",
    )
