from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship
from sqlalchemy import String


from app.db.models.base import Base

if TYPE_CHECKING:
    from .user import User
    from .user_character import UserCharacter


class Item(Base):
    item_name: Mapped[str] = mapped_column(
        String(50),
        nullable=False,
        unique=True,
    )
    item_type: Mapped[str] = mapped_column(
        String(50),
        nullable=False,
    )
    health_points_bonus: Mapped[int] = mapped_column(
        default=0,
        server_default="0",
    )
    speed_bonus: Mapped[int] = mapped_column(
        default=0,
        server_default="0",
    )
    damage_bonus: Mapped[int] = mapped_column(
        default=0,
        server_default="0",
    )
    cost: Mapped[int] = mapped_column(default=1, server_default="1")

    users: Mapped[list["User"]] = relationship(
        secondary="user_item_association_table",
        back_populates="items",
    )

    user_characters: Mapped[list["UserCharacter"]] = relationship(
        secondary="character_item_association_table",
        back_populates="items",
    )
